# Exercices de SPARQL pour Coding Dojo - 11 Mai 2023

Dans ce tutoriel, nous allons vous faire exercer les concepts théoriques introduits précédemment sur des ensembles de données de recherche hébergés en ce moment à l'Université de Genève au sein de [hedera](https://www.unige.ch/eresearch/fr/services/hedera/).

## Partie 1: La Correspondance de Jean-Alphonse Turrettiini

Le premier groupe d'exercices portera sur la correspondance de Jean-Alphonse Turrettini, théologien genevois du XVIIIème qui a entretenu une correspondance épistolaire avec d'autres théologiens et philosophes de l'époque. Cet ensemble de données est le fruit du travail de la Prof. Maria-Cristina Pitassi de l'institut d'histoire de la réforme. Pour découvrir le projet, vous pouvez visiter ce site: https://humanities.unige.ch/turrettini/

L'ontologie pour cet ensemble de donnée a été conçue de manière ad-hoc à l'aide du logiciel "[protégé](https://protege.stanford.edu/)". [Un export en RDF-XML de cette ongologie est accessible ici](http://lod.unige.ch/ontologies/turrettini).

Le schéma ci-dessous donne une vision d'ensemble du modèle de données établi pour les données Turrettini. Ce sont principalement le lien entre les différentes classes qui y sont visualisés, les propriété des données bruts ne le sont pas. Le préfixe pour chaque propriété et classe est "http://lod.unige.ch/ontologies/turrettini#"

![Modèle de données de Turrettini global](/imgs/turrettiniDataModel.png "Classes du modèle de turrettini et les relation entre elles")

Le schéma suivant est une visualisation générée automatiquement par le programm WebVOWL en lui donnant le fichier d'ontologie en entrée,  il montre l'entiereté des propriétés reliant chaque classe à ses données brutes ou à d'autres classes. A noter que les noms des propriété et des classes affichées ne sont pas les noms d'usage en SPARQL; ce sont juste leur description telle que référencée dans le fichier d'ontologie. 

![Modèle de données "complet" de Turrettini](/imgs/turrettiniVOWL.png "Toutes les propriétés et relations de l'ontologie turrettini")

Les données en RDF du projet Turrettini sont accessibles par l'endpoint SPARQL "https://sparql.unige.ch/rest/turrettini/query". Cette URI n'est pas sensé mener à une quelconque page web, elle sert de porte d'entrée aux données RDF Turrettini enregistré dans le triplestore d'hedera. Pour interagir avec les données nous avons besoin d'un "client SPARQL", dans cet atelier nous utiliserons https://yasgui.triply.cc/.

Ouvrez https://yasgui.triply.cc/ dans un nouvel onglet, copiez-collez "https://sparql.unige.ch/rest/turrettini/query" dans le champ d'entrée au sommet à gauche de l'écrou, appuyez sur Enter. YASGUI est à présent configuré pour recevoir des requêtes pour les données en RDF du projet Turrettini.

### Préambule: explorer le modèle de données sans ontologie et retrouver les propriétés liées à une classe 
Bien que les ressources listées précédemment servent de références pour comprendre et utiliser l'ontologie Turrettini, il est parfois pratique d'obtenir ces informations directement par le biais de requêtes SPARQL. Cette requête permet notamment d'obtenir la liste des différentes classes utilisées dans les données:
```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?class WHERE {
  ?subject a ?class .
}
```

Vous pouvez ensuite sélectionner une classe, par exemple "Location" et voir toutes ses propriétés sortantes avec cette requête:

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?outgoing_prop WHERE {
  ?subject a t:Location .
  ?subject ?outgoing_prop ?obj 
}
```

Pour les propriétés rentrantes de "Location" et leur classes respectives, la requête suivante peut être utilisée:

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?ingoing_prop ?ingoing_class WHERE {
  ?subject a t:Location .
  ?obj ?ingoing_prop ?subject . 
  ?obj a ?ingoing_class
}
```

La fonctionnalité "Describe" peut aussi être utilisée pour obtenir des informations complémentaires sur les classes et les proporiétés.

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

DESCRIBE t:Location
```

Dans l'éventualité où votre requête ne produit aucuns résultats, il est possible que vous ayez fait une simple faute de frappe dans le nom de la propriété que vous souhaitez utiliser pour relier deux classes sémantiques entre elles ou bien une classe à une de ses données brutes. Les requêtes précédentes peuvent de la sorte vous débloquer. 

### Exercice 1.0

Pour vous donner une base sur laquelle démarrer cette série d'exercice, voici une requêtes SPARQL qui affiche la liste des différents lieux référencés dans les données:

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?name WHERE {
  ?subject a t:Location . 
  ?subject t:locationName ?name .
}
```

Testez cette requête dans YASGUI, assurez vous que vous obtenez bien une liste de lieux en résultat. Puis modifiez la requête pour qu'elle vous affiche la liste des _Personnes_ référencées dans l'ensemble donnée, affichant le nom et le prénom de chacune d'entre elles.

<details>
 <summary markdown="span">Indice:</summary>

Si vous avez de la peine à sélectionner la bonne classe et les bonnes propriétés pour obtenir le résultat que vous désirez, utilisez les requêtes d'aide listées dans le préambule.

</details>


<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?firstName ?name WHERE {
  ?subject a t:Person . 
  ?subject t:name ?name .
  ?subject t:firstName ?firstName 
}
```
</details>

### Exercice 1.1
Quel est l'URI du sujet RDF lié à la personne qui s'appelle "Jean-Alphonse Turrettini" ?
<details>
 <summary markdown="span">Indice:</summary>

Reprenez la requête que vous avez modifié dans l'exercice précédent, puis utilisez le pattern `filter(regex(?name, "turrettini", "i"))` pour filtrer les résultats.

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?subject ?firstName ?name WHERE {
  ?subject a t:Person . 
  ?subject t:name ?name .
  ?subject t:firstName ?firstName 
  filter(regex(?firstName, "jean-alphonse", "i"))
  filter(regex(?name, "turrettini", "i"))
}
```
</details>

### Exercice 1.2
En vous basant uniquement sur les données RDF, en quelle année est mort Jean-Alphone Turrettini ?
<details>
 <summary markdown="span">Indice:</summary>

Utilisez le résultat de la requête précédente pour d'abord sélectionner le bon sujet puis trouvez la propriété se rapportant à la date de mort d'une personne.

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT ?date WHERE {
  bind(<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
  ?subject a t:Person . 
  ?subject t:deathYear ?date .
}

```
</details>

### Exercice 1.3 
En vous basant uniquement sur les données RDF, quel âge avait Turrettini au moment de sa mort ? Son âge doit être affiché comme résultat de la requête SPARQL.
<details>
 <summary markdown="span">Indice:</summary>

N'oubliez pas que les opérations arithmétiques sont supportées en SPARQL.

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT ?date WHERE {
  bind(	<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
  ?subject a t:Person . 
  ?subject t:deathYear ?death .
  ?subject t:birthYear ?birth .
  bind(?death - ?birth as ?date)
}

```
</details>

### Exercice 1.4
Quel est le nombre de lettres _envoyées_ par Jean-Alphonse Turrettini ?
<details>
 <summary markdown="span">Indice:</summary>

Etudiez les propriétés qui relient la classe Lettre et Personne, puis utilisez l'opérateur "count". 

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT (count(distinct ?letter) as ?count) WHERE {
  bind(	<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
  ?letter t:hasSender ?subject .
}

```
</details>

### Exercice 1.5
Quel est le nombre de lettres _reçues_ par J-A Turrettini provenant d'une personne qui n'est _pas_ Auteur ?
<details>
 <summary markdown="span">Indice:</summary>

Réutilisez la requête précédente mais changez le lien sémantique entre la lettre et Turrettini. Utilisez soit "filter" soit "not exists" pour filtrer les résultats selon si la personne est auteur.

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT (count(distinct ?letter) as ?count) WHERE {
  bind(	<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
  ?letter t:hasRecipient ?subject .
  ?letter t:hasSender ?sender .
  NOT EXISTS {
  	?sender t:isAuthor ?auth_bool
 } # A noter qu'un filter avec négation sur ?auth_bool fonctionnerait également, cette requête marche car seul les personnes qui sont auteurs ont la propriété "isAuthor", l'object du triple étant toujours "true".
}
```
</details>

### Exercice 1.6
Etablissez le classement des 10 "amis" préférés de Turrettini. C'est à dire les 10 personnes qui ont le plus reçu ses lettres.
<details>
 <summary markdown="span">Indice:</summary>

Groupez le nombre de lettres selon les personnnes qui l'ont reçues.

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT (count(distinct ?letter) as ?count) ?name ?firstname  WHERE {
  bind(	<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
    ?letter t:hasSender ?subject .
    ?letter t:hasRecipient ?correspondant .
  	?correspondant t:name ?name .
  	?correspondant t:firstName ?firstname .
} group by ?name ?firstname  order by desc(?count) limit 10

```
</details>

### Exercice 1.7
Quel était le nom du correspondant de J-A Turrettini qui a survécu le plus longtemps (i.e. dont la date de décès était la plus récente)?
<details>
 <summary markdown="span">Indice:</summary>

Pensez au fait qu'un "correspondant de J-A Turrettini" signifie soit une personne qui a reçu des lettres du théologien, soit qui lui en a envoyé ou bien les deux. 

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql

PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?name ?firstname ?year WHERE {
  bind(	<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
  { ?letter t:hasRecipient ?subject .
    ?letter t:hasSender ?correspondant }
  union {
    ?letter t:hasSender ?subject .
    ?letter t:hasRecipient ?correspondant
  }
  ?correspondant t:name ?name .
  ?correspondant t:firstName ?firstname .
  ?correspondant t:deathYear ?year
} order by desc(?year) limit 1
#A noter qu'une implémentation avec "max(?year)" fonctionnerait.


```
</details>

### Exercice 1.8
Quel était le nom et l'âge du correspondant de J-A Turrettini qui était le plus jeune au moment d'avoir reçu pour la première fois une lettre de ce dernier?
<details>
 <summary markdown="span">Indice:</summary>

Inspirez vous des requêtes de l'exercice 1.3 et 1.7

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX t: <http://lod.unige.ch/ontologies/turrettini#>

SELECT distinct ?firstname ?name ?age WHERE {
  bind(	<https://lod.unige.ch/rest/turrettini/person/ug49198> as ?subject)
  { ?letter t:hasRecipient ?subject .
    ?letter t:hasSender ?correspondant }
  union {
    ?letter t:hasSender ?subject .
    ?letter t:hasRecipient ?correspondant
  }
  ?letter t:letterYear ?letterYear .
  ?correspondant t:name ?name .
  ?correspondant t:firstName ?firstname .
  ?correspondant t:birthYear ?year
  bind(?letterYear - ?year as ?age)
} order by ?age limit 1
#de manière similaire au 1.7, une implémentation avec "min(?age)" est possible

```
</details>


## Partie 2: le Projet Visual Contagions

[Visual Contagions](https://www.unige.ch/visualcontagions/) est un projet FNS de recherche en histoire de l'art mené par la Prof. Béatrice Joyeux-Prunel. Le projet se penche sur le sujet de la circulation des images d'art à grande échelle, en utilisant des techniques de Machine Learning afin de grouper ensemble les images similaires parmis les 3.6 millions de scans qui y sont référencés. Dans les données RDF que nous étudierons aujourd'hui, nous nous intéresserons aux magazines, journaux et périodiques du XXè siècle, dont les images et métadonnées ont été moissonnées au cours des 2 dernières années. 

A la différence de la correspondance de J-A Turrettini, les chercheurs de Visual Contagions ont choisi d'utiliser une ontologie préexistante comme source des classes et propriétés de son modèle de données RDF. L'ontologie choisie est le [CIDOC-CRM](https://www.cidoc-crm.org/), vocabulaire sémantique souvent utilisé dans le domaine des humanités. Cette ontologie est basée sur le principe d'évènements ; de la sorte, le sous-ensemble du modèle de données qus nous considérerons dans ces exercices comporte plus de classes que celui de Turrettini. Certains concepts qui n'étaient qu'une simple propriété avec valeur brute dans le modèle Turrettini (comme par exemple la date d'une lettre) sont ici représenté par un enchaînement de proporiété-class-propriété (Comment la date d'un évènement de publication a besoin d'une classe intermédiaire "crm:E52_Time-Span" par exemple). C'est le prix à payer pour rendre les données interopérables et augmenter leur ouverture vers le monde extérieur. 

![Portion du modèle de données de VisualContagions global](/imgs/visualcontagionsSubset.png "Modèle de données Visual Contagions avec ses propriétés et classes")

Avant de démarrer les exercices, nous souhaitons vous rendre bien attentifs à certaines particularités propres au CIDOC-CRM qu'on retrouve dans cet ensemble de données:
1. Contrairement à Turrettini, les noms des différentes entités (journaux & numéros) ne sont pas une simple propriété reliée à la classe correspondante avec une string au bout. Il y a une classe intermédiaire "crm:E41_Appellation" reliée à l'entité par la propriété "crm:P1_is_identified_by", et reliée à la string contenant le nom par la propriété "crm:P190_has_symbolic_content". 
2. Les dates des numéros sont encodés en format de "rangée de temps". Parfois la date d'un numéro peut être imprécise, par exemple imaginons que pour une publication donnée, nous savons tout juste qu'elle s'est passée en Juillet 1995 sans indication du jour. Pour correctement encoder cette incertitude, en CIDOC nous décrivons cette date comme un "Time-Span" qui a un début (propriété "crm:P82a_begin_of_the_begin") et une fin (propriété "crm:P82b_end_of_the_begin"). La valeur de "Juillet 1995" devient donc une instance de la classe  crm:E52_Time-span dont le p82a a la valeur de "1995-07-01T00:00:00+00:00" et le p82b a la valeur de "1995-07-31T23:59:59+00:00". Cette logique de time-span persiste même si on a un jour exact pour le numéro, dans le cas où l'information encodée est le "6 Juillet 1995" le p82a aura comme valeur "1995-07-06T00:00:00+00:00" tandis que le P81b aura comme valeur "1995-07-06T23:59:59+00:00".


### Exercice 2.0
Avant toute chose, n'oubliez pas de changer dans YASGUI l'endpoint SPARQL par "https://sparql.unige.ch/rest/visualcontagions/query" et d'appuyer sur Enter pour configurer le client sur les données de Visual Contagions.

Ci-dessous est une requête SPARQL qui vous affiche une liste de sujets RDF qui correspondent aux numéros d'un journal dont le sujet RDF a été associé à la variable "?serial_work". Lancez la requête et confirmez que vous obtenez bien 55 URIs différents

```sparql
PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

select distinct ?man_made_thing where {
    bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8081259> as ?serial_work)
  	?serial_work a frbroo:F18_Serial_Work .
  	?publication_work frbroo:R23_created_a_realization_of ?serial_work .
    ?publication_work crm:P108_has_produced ?man_made_thing .
    ?man_made_thing a crm:E24_Physical_Human-Made_Thing . 
}
```

Ensuite, complétez/modifiez cette requête pour obtenir le titre (donnée `issue title` dans le modèle de donnée) de chacun de ces numéros. Quel semble être le nom du journal dont vous avez obtenu la liste des numéros ?


<details>
 <summary markdown="span">Indice:</summary>

  Soyez attentif au fait qu'il y a une classe intermédiaire entre un "Physical_Human-Made_Thing" et son titre.

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>

select distinct ?issue_title where {
    bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8081259> as ?serial_work)
  	?serial_work a frbroo:F18_Serial_Work .
  	?publication_work frbroo:R23_created_a_realization_of ?serial_work .
    ?publication_work crm:P108_has_produced ?man_made_thing .
    ?man_made_thing a crm:E24_Physical_Human-Made_Thing . 
  	?man_made_thing crm:P1_is_identified_by ?issue_appellation . 
  	?issue_appellation crm:P190_has_symbolic_content ?issue_title
}

```
</details>

### Exercice 2.1
Sans passer par les "issue" (classe E24_Physical_Human-Made_Thing) et leur titres, quel est le nom du journal dont le sujet en RDF est "<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537>" ?

<details>
 <summary markdown="span">Indice:</summary>

Ne confondez pas le journal (classe frbroo:F18 Serial Work) avec l'évènement de publication d'un numéro de ce journal (classe frbroo:F30_Publication_Event). 

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>

select distinct ?journal_title where {
    bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537> as ?serial_work)
  	?serial_work a frbroo:F18_Serial_Work .
  	?serial_work crm:P1_is_identified_by ?serial_appellation . 
    ?serial_appellation a crm:E41_Appellation .
  	?serial_appellation crm:P190_has_symbolic_content ?journal_title
}

```
</details>

### Exercice 2.2
Affichez la "fin du début" (donnée `Range Date End` dans le schéma) de la date de publication de chacun des numéros de ce journal publié en 1945. Basé sur les résultats, quel est à votre avis le rhythme de parution (journalier, hebdomadaire, mensuel, annuel,...) du journal ? 


<details>
 <summary markdown="span">Indice:</summary>

  Si vous avez de la peine à filtrer selon la date, n'oubliez pas que vous pouvez extraire que l'année d'une date avec la fonction `year(?var)`

</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>

select distinct ?publication_work ?end_date where {
  bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537> as ?serial_work)
  ?serial_work a frbroo:F18_Serial_Work .
  ?publication_work frbroo:R23_created_a_realization_of ?serial_work .
  ?publication_work crm:P4_has_time-span ?ts .
  ?ts crm:P82b_end_of_the_begin ?end_date .
  filter(year(?end_date) < 1946 && year(?end_date) > 1944)
}

```
</details>

### Exercice 2.3

### Partie a: la clé wikidata

Encore et toujours sur le même journal de l'exo 2.1, trouvez le lien wikidata associé à la ville d'origine de l'organisation qui se charge de publier ce journal. 

<details>
 <summary markdown="span">Indice:</summary>
  Faites bien attention aux différences de préfixe pour obtenir le lien wikidata lié à la ville.
</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>

select distinct ?wikidata_id where {
  bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537> as ?serial_work)
  ?serial_work a frbroo:F18_Serial_Work .
  ?publication_work frbroo:R23_created_a_realization_of ?serial_work .
  ?publication_work crm:P14_carried_out_by ?actor .
  ?actor crm:P74_has_former_or_current_residence ?city .
  ?city crmdig:L54_is_same-as ?wikidata_id 
}

```
</details>

#### Partie b: Federated Queries
La présence de liens wikidata dans les données RDF est loin d'être anodine, cet URL constitue une clé vers le monde extérieur. Wikidata dispose également de son triplestore pouvant recevoir des requêtes SPARQL par le biais de l'URL "https://query.wikidata.org/bigdata/namespace/wdq/sparql". Il est possible d'indiquer à notre triplestore de faire des sous-requêtes sur wikidata pour de la sorte obtenir des résultats combinés entre les données de Visual Contagions et celles de wikidata. Si le lien wikidata dans la partie "a" de cet exercice était obtenu par la variable "?ẁikidata_id" rajouter ce bout de requête à celle que vous avez écrit dans la partie a vous permet de récupérer les propriétés et objet associé au sujet RDF que vous avez extrait dans la partie a.

```sparql
service <https://query.wikidata.org/bigdata/namespace/wdq/sparql> { 
        ?wikidata_id ?prop ?obj
  }
```
Si jamais votre requête prend trop de temps à compléter rajoutez un "limit 20" à la fin pour réduire le nombre de données renvoyées par wikidata.
En explorant un peu les chemins RDF du côté de wikidata, seriez vous capable de retrouver quel est le nom de la ville référencée ?


<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>

select distinct ?prop ?obj where {
  bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537> as ?serial_work)
  ?serial_work a frbroo:F18_Serial_Work .
  ?publication_work frbroo:R23_created_a_realization_of ?serial_work .
  ?publication_work crm:P14_carried_out_by ?actor .
  ?actor crm:P74_has_former_or_current_residence ?city .
  ?city crmdig:L54_is_same-as ?wikidata_id 
  service <https://query.wikidata.org/bigdata/namespace/wdq/sparql> { 
    ?wikidata_id ?prop ?obj
  }
} limit 20


```
</details>


### Exercice 2.4
En vous aidant du lien wikidata obtenu dans la question précédente, et en utilisant uniquement le RDF comme outil, en 1950, quelle était la population de la ville de parution du journal ? Pour vous aider, nous vous donnons cette requête SPARQL incomplète dans laquelle les pattern vers la donnée "population" et la donnée "date" du côté de wikidata ont déjà été isolé dans les variables correspondantes.

```sparql
prefix wprop: <http://www.wikidata.org/prop/>
prefix wprops: <http://www.wikidata.org/prop/statement/>
prefix wpropsv: <http://www.wikidata.org/prop/statement/value/>
prefix wpropq: <http://www.wikidata.org/prop/qualifier/>

  service <https://query.wikidata.org/bigdata/namespace/wdq/sparql> { 
        ?wikidata_id wprop:P1082 [
          wprops:P1082 ?population ;
          wpropq:P585 ?date
        ]
  }
```
<details>
 <summary markdown="span">Indice:</summary>

  Inspirez vous de l'exercice 2.2

</details>


<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>
prefix wprop: <http://www.wikidata.org/prop/>
prefix wprops: <http://www.wikidata.org/prop/statement/>
prefix wpropsv: <http://www.wikidata.org/prop/statement/value/>
prefix wpropq: <http://www.wikidata.org/prop/qualifier/>

select distinct ?country ?wd ?population ?date where {
  bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537> as ?serial_work)
  ?serial_work a frbroo:F18_Serial_Work .
  ?publication_work frbroo:R23_created_a_realization_of ?serial_work .
  ?publication_work crm:P14_carried_out_by ?actor .
  ?actor crm:P74_has_former_or_current_residence ?city .
  ?city crmdig:L54_is_same-as ?wikidata_id 
  service <https://query.wikidata.org/bigdata/namespace/wdq/sparql> { 
    ?wikidata_id wprop:P1082 [
        wprops:P1082 ?population ;
        wpropq:P585 ?date
    ]
    filter(?date > "1940-01-01T00:00:00Z"^^xsd:dateTime && ?date < "1950-12-31T23:59:59Z"^^xsd:dateTime)
  } 
}


```
</details>

### Exercice 2.5
Toujours sur le même journal, quel personnage historique est présent sur la couverture de l'avant dernier numéro d'Avril 1945 ?


<details>
 <summary markdown="span">Indice:</summary>
  Bien évidemment, une image n'est pas une donnée qu'on peut retrouver dans le RDF pur, analysez le modèle de donnée et identifiez quel propriété pourrait vous mener à la couverture d'un numéro de journal.
</details>

<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>

select distinct ?cover_url where {
  bind(<https://lod.unige.ch/rest/visualcontagions/actor/ug8101537> as ?serial_work)
  ?serial_work a frbroo:F18_Serial_Work .
  ?publication_work frbroo:R23_created_a_realization_of ?serial_work .
  ?publication_work crm:P4_has_time-span ?ts .
  ?publication_work crm:P108_has_produced ?issue .
  ?issue a crm:E24_Physical_Human-Made_Thing .
  ?issue crm:P1_is_identified_by ?ident .
  ?ident rdf:value ?cover_url .
  ?ts crm:P82b_end_of_the_begin ?end_date .
  filter(?end_date < "1945-04-24T23:59:59"^^xsd:dateTime && ?end_date > "1945-04-17T00:00:00"^^xsd:dateTime)
}

```
</details>

### Exercice 2.6
En utilisant les coordonnées GPS liées au lieu, réalisez une cartographie des différentes journaux selon les coordonnées GPS liées au lieu d'origine de parution. Pour réaliser une cartographie avec yasgui, voici le template de la requête SPARQL:

```sparql
SELECT ?x ?xLabel WHERE  {
  
  #triple patterns here ...

}
```

Dans cette requête, la variable ?x doit contenir les coordonnées GPS, tandis que la variable ?xLabel doit contenir le nom du journal. Une fois que votre requête renvoie ces deux informations pour chaque journal référencé dans les données VC, cliquez sur l'onglet "Geo" de yasgui pour afficher votre cartographie. Quel continent semble être le plus représenté dans les données de VisualContagions ?


<details>
 <summary markdown="span">Solution Possible:</summary>

```sparql
PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo#>

select distinct ?xLabel ?x where {
  ?serial_work a frbroo:F18_Serial_Work .
  ?serial_work crm:P1_is_identified_by ?serial_appellation . 
  ?serial_appellation a crm:E41_Appellation .
  ?serial_appellation crm:P190_has_symbolic_content ?xLabel .
  ?publication_work frbroo:R23_created_a_realization_of ?serial_work .
  ?publication_work crm:P14_carried_out_by ?actor .
  ?actor crm:P74_has_former_or_current_residence ?city .
  ?city crm:P168_place_is_defined_by ?x
}

```
</details>